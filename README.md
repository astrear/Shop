# Shop Services

Services in python using django and dajngo REST framework.

## Installation

Setup project
```bash
pip install pipenv
git clone https://gitlab.com/astrear/Shop
cd Shop
```

Install dependencies
```bash
pipenv install
pipenv shell
```

Setup database
```bash
cd shop_api
python manage.py makemigrations
python manage.py migrate
```

Create a superuser to login
```bash
python manage.py createsuperuser
```

Run server
```bash
python manage.py runserver 0.0.0.0:80
```
[Endpoints documentation](http://127.0.0.1:8000/docs/).

## License
[MIT](https://choosealicense.com/licenses/mit/)