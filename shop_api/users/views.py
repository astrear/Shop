from users.models import User
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.reverse import reverse
 
from users.serializers import UserSerializer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
 
class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
 
 
class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer