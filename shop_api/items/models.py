from django.db import models
 
class Item(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False, null=False)
    image = models.CharField(max_length=200, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
 
    class Meta:
        ordering = ('created',)