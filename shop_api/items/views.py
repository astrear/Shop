from items.models import Item
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.reverse import reverse
 
from items.serializers import ItemSerializer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import IsAuthenticated

class ItemList(generics.ListCreateAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)
    
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
 
 
class ItemDetail(generics.RetrieveDestroyAPIView):
    authentication_classes = ((JSONWebTokenAuthentication,))
    permission_classes = (IsAuthenticated,)

    queryset = Item.objects.all()
    serializer_class = ItemSerializer